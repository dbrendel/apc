# Another Price Checker - apc

This little tool has been created to learn more about using libcurl.

It has only been tested against a widely used German online shop where it should be able to extract product title and price.

Again: this is just a tool for learning purposes, don't put it into scripts.

# How to build

## Requirements

- git
- cmake
- libcurl-devel
- C compiler

## Get the sources

Just clone the repository:

```bash
$ git clone https://gitlab.com/dbrendel/apc.git
```

## Prepare the build

Now create a directory for out-of-tree builds:

```bash
$ mkdir apc-build
$ cd apc-build
```

## Build

Now you can configure and build:

```bash
$ cmake ../apc/
$ make
```

# How to use

## Local server

In order to leave the shop alone you can download a copy of any product web page and serve it locally for testing purposes.

`nc` is required for doing it like in this example.

```bash
$ while true; do nc -l -p 1500 -c 'echo -e "HTTP/1.1 200 OK\n\n$(cat SHOP.de_dumped.html)"'; done
$ ./apc localhost 1500
```

## The real deal

You should not do that too often, but it works the same way.

```bash
$ ./apc https://www.exampleshop.de/FOO-product/dp/A01ABXXX9/
```

