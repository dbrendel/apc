/*  Copyright (C) 2023 Dennis Brendel

    This file is part of another price checker "apc".

    apc is free software: you can redistribute it and/or modify it under the
    terms of the GNU General Public License as published by the
    Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    apc is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
    details.

    You should have received a copy of the GNU General Public License along
    with apc. If not, see <https://www.gnu.org/licenses/>. */

#include "apc.h"

#include <string.h>

size_t get_data_cb(char *_data, size_t _size, size_t _nmemb, void *_userdata)
{
    size_t real_size = _size * _nmemb;
    struct chunk *response = (struct chunk*) _userdata;

    char *ptr = realloc(response->data, response->size + real_size + 1);
    if (ptr == NULL)
    {
        // != real_size will trigger an error in libcurl
        return 0;
    }

    response->data = ptr;
    memcpy(&(response->data[response->size]), _data, real_size);

    response->size += real_size;
    response->data[response->size] = '\0';

    return real_size;
}

char *get_value(const char *_data, const char *_key, char _sep, size_t _val_strlen)
{
    const size_t len_key = strlen(_key)+ 1;
    const size_t len_res = _val_strlen + 1;

    char *start;

    char *result = malloc(len_res);

    if (result == NULL)
    {
        return NULL;
    }
    result = memset(result, '\0', len_res);

    start = strstr(_data, _key);
    if (start != NULL)
    {
        // consider trailing '\0'
        start += len_key - 1;
        for (size_t i = 0; i < len_res; i++)
        {
            if (start[i] != _sep)
            {
                result[i] = start[i];
            }
        }
    }
    
    return result;
}

