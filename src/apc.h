/*  Copyright (C) 2023 Dennis Brendel

    This file is part of another price checker "apc".

    apc is free software: you can redistribute it and/or modify it under the
    terms of the GNU General Public License as published by the
    Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    apc is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
    details.

    You should have received a copy of the GNU General Public License along
    with apc. If not, see <https://www.gnu.org/licenses/>. */

#include <stdlib.h>

struct chunk {
    char *data;
    size_t size;
};

size_t get_data_cb(char *_data, size_t _size, size_t _nmemb, void *_userdata);
char *get_value(const char *_data, const char *_key, char _sep, size_t _val_strlen);

