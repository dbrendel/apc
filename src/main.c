/*  Copyright (C) 2023 Dennis Brendel

    This file is part of another price checker "apc".

    apc is free software: you can redistribute it and/or modify it under the
    terms of the GNU General Public License as published by the
    Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    apc is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
    details.

    You should have received a copy of the GNU General Public License along
    with apc. If not, see <https://www.gnu.org/licenses/>. */

#include "apc.h"

#include <stdlib.h>
#include <string.h>

#include <curl/curl.h>

struct chunk response = { NULL };

int main(int _argc, char **_argv)
{
    const char* user_agent = "Mozilla/5.0 (X11; Linux x86_64; rv:109.0) "
                             "Gecko/20100101 Firefox/112.0";

    char *url = "";
    long port = 0;

    CURL *curl = NULL;
    CURLcode res = CURLE_OK;
    size_t len = 0;
    char errbuf[CURL_ERROR_SIZE];

    char *value = NULL;
    double price = 0.0;

    if (_argc > 1)
    {
        url = _argv[1];
    }

    if (_argc == 3)
    {
        port = atoi(_argv[2]);
    }

    errbuf[0] = '\0';

    curl = curl_easy_init();
    if (curl == NULL)
    {
        fprintf(stderr, "Error, could not initialize curl!\n");
        return EXIT_FAILURE;
    }
    curl_easy_setopt(curl, CURLOPT_URL,             url);
    curl_easy_setopt(curl, CURLOPT_PORT,            port);
    curl_easy_setopt(curl, CURLOPT_USERAGENT,       user_agent);
    curl_easy_setopt(curl, CURLOPT_ERRORBUFFER,     errbuf);
    curl_easy_setopt(curl, CURLOPT_ACCEPT_ENCODING, "");
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION,   get_data_cb);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA,       (void *) &response);

    res = curl_easy_perform(curl);
    if (res != CURLE_OK)
    {
        len = strlen(errbuf);
        if (len > 0)
        {
            fprintf(stderr, "%s\n", errbuf);
        }
        else
        {
            fprintf(stderr, "%s\n", curl_easy_strerror(res));
        }

        return EXIT_FAILURE;
    }

    curl_easy_cleanup(curl);

    if (response.data == NULL)
    {
        return EXIT_FAILURE;
    }

    value = get_value(response.data, "productTitle\":\"", '\"', 128);
    if (value != NULL)
    {
        printf("Product: %s\n", value);
        free(value);
        value = NULL;
    }

    value = get_value(response.data, "priceAmount\":", ',', 16);
    if (value != NULL)
    {
        price = strtod(value, NULL);
        printf("Price: %.2f\n", price);

        free(value);
        value = NULL;
    }

    return EXIT_SUCCESS;
}

