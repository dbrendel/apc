/*  Copyright (C) 2023 Dennis Brendel

    This file is part of another price checker "apc".

    apc is free software: you can redistribute it and/or modify it under the
    terms of the GNU General Public License as published by the
    Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    apc is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
    details.

    You should have received a copy of the GNU General Public License along
    with apc. If not, see <https://www.gnu.org/licenses/>. */

#include "apc.h"

#include <stdio.h>
#include <string.h>

int tst_get_value(void)
{
    const char *data = "abcd>Identifier\":12.34,next1234";
    const char *key = "Identifier\":";
    const char *expected = "12.34";
    const char sep = ',';
    const size_t len = 5;
    char *value = NULL;
    int res = 0;

    value = get_value(data, key, sep, len); 

    if (value == NULL)
    {
        fprintf(stderr, "Failed to get value!\n");

        res++;
    }
    else
    {
        if (strlen(value) > len)
        {
            fprintf(stderr, "Expected: %d\n"
                            "Got:      %d\n", len, strlen(value));
            res++;
        }
        if (strncmp(value, expected, len) != 0)
        {
            fprintf(stderr, "Expected: %s\n"
                            "Got:      %s\n", expected, value);
            res++;
        }
    }

    free(value);

    return res;
}

int main(void)
{
    int res = 0;

    res += tst_get_value();

    return res;
}

